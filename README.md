![slide] (http://res.cloudinary.com/comm1t/image/upload/v1449126545/piatto2_oohxyd.jpg "Slide")

# Psd To Html, CSS3, jQuery, Bootstrap, Animate etc.
http://kt815.bitbucket.org/piatto.psd/
(shorten url http://goo.gl/BJdDXV)

## Source: Piatto – A Free Flat Style Landing Page PSD
http://wegraphics.net/downloads/piatto-a-free-flat-style-landing-page-psd/

## Installation:

1. `npm install`

2. `bower install`

## Build tasks

`grunt:copy`
`grunt`
`grunt:prod`

## File & Folder structure

* **dist/**
    * **css/**
        * piatto.min.css
    * **js/**
        * app.min.js
        * wow.min.js
    * **fonts/** *(@font-face)* chunkfive-webfont
    * **fav/** *(favicons)*
    * **images** *(optimized images)*
    * **assets** *(photohop sources)*
    * index.html
    * browserconfig.xml *(favicons config)*

## Checklist
https://www.forgett.com/checklist/2725109886

## Static Pages (the final work)
http://kt815.bitbucket.org/piatto.psd/

## Вёрстка макета

* **PSD Макет** находиться по ссылке http://goo.gl/S6UeJ7
* **Исходный код на bitbucket**: https://goo.gl/VF3BqJ
* В качестве менеджера задач используется **grunt**
* Выполнена **HTML верстка** макета
* Основные инструменты Jade, Less, Npm, Bower, Grunt + tasks, Git, Photoshop
* В вёрстке использовались **HTML5, CSS3, jQuery, Bootstap, Animate.css**
* **Bootstrap 3.3.5 only grid and responsive, normalize.css**: https://goo.gl/7bCU4r
* Кросс-браузерный **@font-face**
* Добавлены **favicons для разных устройств, браузеров, ОС**
* **Анимация**: при прокуртке страницы; при наведении на изображение
* Responsive вёрстка
* **jQuery**, jquery.leanModal.js, browser window scroll + "back to top"
* **Блочная** (div)
* **Кроссбраузерность**: IE8+ (тестировался на виртуальных машинах Microsoft https://goo.gl/tgmy6L), Chrome, Firefox, Opera, Safari 5+
* Валидно (**W3C**)
* **SEO-friendly**
* **Оптимизация** (конкатенация css и js; минимизация css, js, html; спрайты для мелких изображений; оптимизация для изображений)
* **Верстка под мобильные** устройства (iOS, Android, Windows, т.д.)
* Вёрстка БЭМ. Блок, Элемент, Модификатор
